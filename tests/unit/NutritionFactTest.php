<?php
use Arolla\Monoid\NutritionFact;

class NutritionFactTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @test
     */
    public function is_it_food() {
        $salad = new NutritionFact('salad', true, -1, 0, true, 10, 1, 1);
        $tomato = new NutritionFact('tomato', true, 10, 1, true, 15, 2, 4);
        $bread = new NutritionFact('bread', true, 200, 30, false, 200, 5, 75);
        $neutral = new NutritionFact('', true, 0, 0, true, 0, 0, 0);

        /** @var NutritionFact $panini */
        $panini = array_reduce([
            $bread, $salad, $tomato, $bread,
        ], function ($panini, NutritionFact $nutritionalFact) {
            return $panini = $panini->combine($nutritionalFact);
        }, $neutral);


        $this->assertEquals(61, $panini->getFat());
        $this->assertEquals(409, $panini->getCalories());
        $this->assertTrue($panini->getVegan());
        $this->assertFalse($panini->getOrganic());
        $this->assertEquals(425, $panini->getWeight());
        $this->assertEquals(425, $panini->getWeight());
        $this->assertEquals(61/425, $panini->getNutritionNumber());
    }
}