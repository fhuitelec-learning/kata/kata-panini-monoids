<?php
namespace Arolla\Monoid;

class NutritionFact
{
    /**
     * @var
     */
    private $name;
    /**
     * @var
     */
    private $vegan;
    /**
     * @var
     */
    private $fat;
    /**
     * @var
     */
    private $organic;

    /**
     * @var float
     */
    private $nutritionNumber;

    /**
     * @var float
     */
    private $weight;

    /**
     * @var float
     */
    private $calories;
    /**
     * @var float
     */
    private $salt;
    /**
     * @var float
     */
    private $sugar;

    /**
     * NutritionFact constructor.
     * @param string $name
     * @param bool $vegan
     * @param int $calories
     * @param float $fat
     * @param bool $organic
     * @param float $weight
     * @param float $salt
     * @param float $sugar
     */
    public function __construct($name, $vegan, $calories, $fat, $organic, $weight, $salt, $sugar)
    {
        $this->name = $name;
        $this->vegan = $vegan;
        $this->calories = $calories;
        $this->fat = $fat;
        $this->organic = $organic;
        $this->weight = $weight;
        $this->salt = $salt;
        $this->sugar = $sugar;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getVegan()
    {
        return $this->vegan;
    }

    /**
     * @return mixed
     */
    public function getFat()
    {
        return $this->fat;
    }

    /**
     * @return mixed
     */
    public function getOrganic()
    {
        return $this->organic;
    }

    /**
     * @return float
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * @return float
     */
    public function getCalories()
    {
        return $this->calories;
    }

    /**
     * @return mixed
     */
    public function getNutritionNumber()
    {
        return (float)$this->fat / $this->weight;
    }

    /**
     * @return float
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * @return float
     */
    public function getSugar()
    {
        return $this->sugar;
    }

    /**
     * @param NutritionFact $nutritionFact
     * @return NutritionFact
     */
    public function combine(NutritionFact $nutritionFact) {
        return new NutritionFact(
            $this->name + $nutritionFact->getName(),
            $this->vegan && $nutritionFact->getVegan(),
            $this->calories + $nutritionFact->getCalories(),
            $this->fat + $nutritionFact->getFat(),
            $this->organic && $nutritionFact->getOrganic(),
            $this->weight + $nutritionFact->getWeight(),
            $this->salt + $nutritionFact->getSalt(),
            $this->sugar + $nutritionFact->getSugar()
        );
    }
}